from Bonds import Bonds
import matplotlib.pyplot as plt

#Check class
investor1 =Bonds("short", 1000)
investor1.calculate_compounded_interest(3, show=True)
investor2 =Bonds("long", 5000)
investor2.calculate_compounded_interest(8, show=True)
print('--------------------------')
##Make plot
list_total_return_investor1 = []
list_total_return_investor2 = []


for year in range(1, 51):
    investor1 =Bonds("short", 250)
    compounded_interest1, total_return1 = investor1.calculate_compounded_interest(year)
    list_total_return_investor1.append(total_return1)
    investor2 =Bonds("long", 1000)
    compounded_interest2, total_return2 = investor2.calculate_compounded_interest(year)
    list_total_return_investor2.append(total_return2)

print(total_return2) # must be equal to 4383,906019

plt.plot(list_total_return_investor1, label = "Short term Bond")
plt.plot(list_total_return_investor2, label = "Long term Bond")
plt.title("Evolution of investisement")
plt.legend()
plt.show()


