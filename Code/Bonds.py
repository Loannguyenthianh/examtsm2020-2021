class Bonds(object):
    def __init__(self, term, invested_amount):
        assert term in ("short", "long"), "YOUR TERM IS INCORRECT,It should be 'short' or 'long' only ! "
        self.term = term
        if self.term=="short":
            self.min_term=2#years
            self.min_amount=250#USD
            self.interest_rate=0.015#1.5%
        else:
            self.min_term = 5  # years
            self.min_amount = 1000  # USD
            self.interest_rate = 0.03  # 3%
        assert invested_amount>=self.min_amount,"PLEASE INVEST AN AMOUNT LARGER THAN THE MINIMUM AMOUNT {}".format(self.min_amount)
        self.invested_amount = invested_amount

    def calculate_compounded_interest(self, time, show=False):
        if time < self.min_term:
            print("Your invested time is shorter than the min required term")
            return 0, self.invested_amount
        else:
            total_return = self.invested_amount*((1+self.interest_rate)**time)
            compounded_interest=total_return - self.invested_amount
            if show:
                print("Your total interest is ${}, and your total return is ${}".format(compounded_interest,total_return))
            return compounded_interest,total_return