from Stocks import Stocks
import yfinance as yf
import datetime as dt
import matplotlib.pyplot as plt


#Check Class
investor1 = Stocks('FDX',25000)
investor1.buy("2016-09-01")
investor2 = Stocks('FDX',50000)
investor2.buy("2016-09-01")
investor3 = Stocks('IBM', 40000)
investor3.sell("2017-02-03", "2019-05-10")
investor4 = Stocks('GOOGL', 100000)
investor4.sell("2017-05-18", "2020-12-10")


###########PLOT ALL THE STOCKS FOR THE ENTIRE PERIOD########
# Build Stock Data Frame
start = dt.datetime(2016, 9, 1)  ##time range
end = dt.datetime(2021, 1, 1)
l_tickers = ['FDX', 'GOOGL', 'IBM', 'KO', 'MS', 'NOK', 'XOM']
stockdf = yf.download(l_tickers, start= start, end= end)['High']

#Plot
stockdf.plot(subplots=True, layout=(3,3), figsize=(30.0, 25.0), grid=True)
plt.show()
