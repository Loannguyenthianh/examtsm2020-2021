import yfinance as yf
import datetime as dt
import pandas as pd
from datetime import datetime

class Stocks(object):
    def __init__(self, name, invested_amount):
        self.name = name
        self.invested_amount = invested_amount


    def buy(self, startdate):

        self.startdate = startdate
        self.enddate =  datetime.strptime(self.startdate,'%Y-%m-%d') + dt.timedelta(days=10)
        stockdf = yf.download([self.name], start=self.startdate, end=self.enddate)['High']
        calendardatedf = pd.date_range(start=self.startdate, end=self.enddate, freq='D')
        calendardatedf = pd.DataFrame(data=calendardatedf, columns=['Date'])
        newstockdata = calendardatedf.merge(stockdf, how='left', right_index=True, left_on='Date')
        newstockdata = newstockdata.fillna(method='ffill')
        number_of_stocks = self.invested_amount / newstockdata['High'][0]
        print("The investor has bought {} number of {} stocks at price: {}/stock".format(number_of_stocks, self.name,newstockdata['High'][0]))
        return number_of_stocks, self.name,newstockdata['High'][0]


    def sell(self,startdate, enddate):

        self.startdate = startdate
        self.enddate = enddate
        stockdf = yf.download([self.name], start=self.startdate, end=self.enddate)['High']
        calendardatedf = pd.date_range(start=self.startdate, end=self.enddate, freq='D')
        calendardatedf = pd.DataFrame(data=calendardatedf, columns=['Date'])
        newstockdata = calendardatedf.merge(stockdf, how='left', right_index=True, left_on='Date')
        newstockdata = newstockdata.fillna(method='ffill')
        number_of_stocks = self.invested_amount / newstockdata['High'][0]
        stockreturn = newstockdata['High'].iloc[-1]-newstockdata['High'][0]
        return_on_investment = ((newstockdata['High'].iloc[-1]-newstockdata['High'][0])/newstockdata['High'][0])*100
        if stockreturn > 0:
            print("The investor has bought {} number of {} stocks at price: {}/stock. He sells at price: {} "
              "He gets a return of ${}/stock. The return on investment is {}%".
              format(number_of_stocks, self.name,newstockdata['High'][0],newstockdata['High'].iloc[-1], stockreturn,return_on_investment))
            return number_of_stocks, self.name,stockreturn,return_on_investment
        else:
            print("The investor has bought {} number of {} stocks at price: {}/stock. He sells at price: {} "
                  "He lost {}$/stock. The return on investment is {}%".
                  format(number_of_stocks, self.name,newstockdata['High'][0],newstockdata['High'].iloc[-1], stockreturn,return_on_investment))
            return number_of_stocks, self.name, stockreturn, return_on_investment




